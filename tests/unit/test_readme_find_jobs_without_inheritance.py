from gcip import Job, Pipeline, JobFilter
from tests import conftest
from gcip.core.sequence import Sequence


def test():
    job = Job(stage="build", script="do_something development")
    job.set_image("foo/bar:latest")

    dev_sequence = Sequence().add_children(job, stage="dev")
    prd_sequence = Sequence().add_children(job, stage="prd")

    dev_sequence.add_tags("dev")
    prd_sequence.add_tags("prd")

    pipeline = Pipeline()
    pipeline.add_children(dev_sequence, prd_sequence)

    # The following filter returns no jobs, as the tags are attributes
    # of the sequences and `find_jobs()` is setup to not look for
    # inherited attributes.

    filter = JobFilter(image="foo/bar:.*", tags="prd")

    for job in pipeline.find_jobs(filter):
        job.set_image("foo/bar:stable")

    conftest.check(pipeline.render())
