from gcip import Job, Pipeline, Sequence
from tests import conftest
from gcip.core.rule import Rule
from gcip.core.cache import Cache
from gcip.core.image import Image
from gcip.core.artifacts import Artifacts


def test_name_population():
    job = Job(name="a", stage="b", script="foobar")
    sequence1 = Sequence().add_children(job, name="c", stage="d")
    sequence2 = Sequence().add_children(sequence1, name="e", stage="f")
    assert sequence2.populated_jobs[0].name == "e-f-c-d-a-b"


def test_initialize_jobs():
    job = Job(
        name="foo",
        stage="bar",
        script="test",
        variables={"foo": "bar"},
        cache=Cache(paths=["./cache/it"]),
        artifacts=Artifacts("take/that"),
        tags=["foobar"],
        rules=[Rule(if_statement="thatworks")],
        image=Image("myimage"),
        allow_failure=True,
    )

    sequence = Sequence().add_children(job)
    sequence.initialize_variables(wrong="value")
    sequence.initialize_artifacts(Artifacts("wrong"))
    sequence.initialize_cache(Cache(paths=["wrong"]))
    sequence.initialize_rules(Rule(if_statement="wrong"))
    sequence.initialize_tags("wrong")
    sequence.initialize_image("wrong")
    sequence.initialize_image(Image("noob"))
    sequence.initialize_allow_failure(False)

    populated_job = sequence.populated_jobs[0]

    assert populated_job.variables == {"foo": "bar"}
    assert populated_job.artifacts.paths[0] == "take/that"
    assert populated_job.cache is not None and populated_job.cache.paths[0] == "./cache/it"
    assert populated_job.rules[0]._if == "thatworks"
    assert "foobar" in populated_job.tags.keys()
    assert populated_job.image is not None and populated_job.image.name == "myimage"
    assert populated_job.allow_failure is True


def test_initialize_sequences():
    job = Job(
        name="foo",
        stage="bar",
        script="test",
    )

    child_sequence = Sequence().add_children(job)
    child_sequence.initialize_variables(foo="bar")
    child_sequence.initialize_artifacts(Artifacts("take/that"))
    child_sequence.initialize_cache(Cache(paths=["./cache/it"]))
    child_sequence.initialize_rules(Rule(if_statement="thatworks"))
    child_sequence.initialize_tags("foobar")
    child_sequence.initialize_image(Image("myimage"))
    child_sequence.initialize_allow_failure(True)

    sequence = Sequence().add_children(child_sequence)
    sequence.initialize_variables(wrong="value")
    sequence.initialize_artifacts(Artifacts("wrong"))
    sequence.initialize_cache(Cache(paths=["wrong"]))
    sequence.initialize_rules(Rule(if_statement="wrong"))
    sequence.initialize_tags("wrong")
    sequence.initialize_image("wrong")
    sequence.initialize_image(Image("noob"))
    sequence.initialize_allow_failure(False)

    populated_job = sequence.populated_jobs[0]

    assert populated_job.variables == {"foo": "bar"}
    assert populated_job.artifacts.paths[0] == "take/that"
    assert populated_job.cache is not None and populated_job.cache.paths[0] == "./cache/it"
    assert populated_job.rules[0]._if == "thatworks"
    assert "foobar" in populated_job.tags.keys()
    assert populated_job.image is not None and populated_job.image.name == "myimage"
    assert populated_job.allow_failure is True


def test_initialize_empty_arrays(pipeline: Pipeline):

    job1 = Job(name="job1", script="date")

    sequence = Sequence().add_children(
        Job(name="job2", script="date").add_dependencies(job1).add_needs(job1),
        Job(name="job3", script="date"),
    )

    pipeline.add_children(
        job1,
        sequence,
        Job(name="job4", script="date").add_dependencies(job1).add_needs(job1),
        Job(name="job5", script="date"),
    )

    pipeline.initialize_dependencies()
    pipeline.initialize_needs()

    conftest.check(pipeline.render())
