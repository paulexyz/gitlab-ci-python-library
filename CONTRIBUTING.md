# Thanks for your interest in our project.

Contributions are welcome. Feel free to [open an issue])(issues) with questions or reporting ideas and bugs,
or [open pull requests](pulls) to contribute code.

Currently this project is an early implementation of an idea with a lot of influences from the daily
use in our pipelines at deutschebahn.com. Not yet ready designed we might still publish breaking changes.

We also have a strong opinion of how this library should be designed. That means not all contributions
would be accepted as-is. Please respect if we enforce changes to contributions or deny some. However
our opinion of how things work are changing, so discussions are welcomed.

We are committed to fostering a welcoming, respectful, and harassment-free environment. Be kind!

## IDE setup hints

To participate onto this project and get into it as quick as possible, we advice to use our Visual Studio Code configuration shipped with this project. To activate:

* Install Docker.
* In VSCode CMD+Shift+P and select "Remote-Containers: Reopen in Container"

Feel free to use any other editor you like, as long as you will ensure following quality requirements before contributing your commits:

* Organizing and sorting imports with `isort`.
* Formatting code with `black`.
* Type checking with `mypy`.
* Unit testing with `pytest`.

The VSCode settings delivered will do most of that automatically (organize imports, formatting, type checking) or setup the required tools (unit testing with code coverage).